const chromium = require('chromium');
const { config } = require('./cucumber.conf');

config.capabilities = [
    {
        browserName: 'chrome',
        acceptInsecureCerts: true,
        'goog:chromeOptions': {
            binary: chromium.path
        },
        'cjson:metadata': {
            browser: {
                name: 'chrome',
                version: 'latest'
            },
            device: 'Local',
            platform: {
                name: 'linux',
                version: 'Mint 20.2'
            }
        },
    },
];

exports.config = config;
