
module.exports = {
    'async-only': true,
    bail: false,
    color: true,
    delay: false,
    diff: true,
    exit: false,
    extension: ['js'],
    'full-trace': true,
    'inline-diffs': true,
    package: './package.json',
    reporter: 'mochawesome',
    'reporter-options': [
        'reportTitle=API Automation Challenge',
        'reportDir=./reports/mochawesome',
        'overwrite=true',
        'charts=true'
    ],
    retries: 1,
    slow: '75',
    sort: false,
    timeout: '20000',
    'trace-warnings': true,
    ui: 'bdd'
};
