const { config } = require('./cucumber.conf');

config.capabilities = [
    {
        browserName: 'firefox',
        'cjson:metadata': {
            browser: {
                name: 'firefox',
                version: 'latest',
            },
            device: 'Local',
            platform: {
                name: 'linux',
                version: 'Mint 22.2'
            }
        },
    },
];

exports.config = config;
