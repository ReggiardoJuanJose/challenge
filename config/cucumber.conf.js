require('dotenv').config();
const env = require('env-var');
const fs = require('fs-extra');
const path = require('path');
const cucumberJson = require('wdio-cucumberjs-json-reporter').default;
const reporter = require('multiple-cucumber-html-reporter');
const logger = require('../tests/utils/logger');
const { suites } = require('../data/wdio.suites');

// Required env vars
const BASE_URL = env.get('BASE_URL').required().asUrlString();

// Optional env vars
const MAX_INSTANCES = env.get('MAX_INSTANCES').asInt() || 1;

const testStart = new Date().toISOString();

exports.config = {
    specs: [
        path.join(__dirname, '../tests/web/features/**/*.feature')
    ],
    suites,
    maxInstances: MAX_INSTANCES,
    waitforTimeout: 25000,
    logLevel: 'error',
    outputDir: './logs',
    bail: 0,
    baseUrl: BASE_URL,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    framework: 'cucumber',
    services: ['chromedriver','geckodriver'],
    reporters: [
        'spec',
        ['cucumberjs-json',
            {
                jsonFolder: './reports/cjson',
            },
        ],
    ],
    cucumberOpts: {
        require: [path.join(__dirname, '../tests/web/steps/**/*.step.js')],
        compiler: [],
        backtrace: false,
        dryRun: false,
        failFast: false,
        format: ['pretty'],
        colors: true,
        snippets: true,
        source: true,
        profile: [],
        strict: false,
        tagExpression: '',
        timeout: 60000,
        ignoreUndefinedDefinitions: true,
    },
    onPrepare: async () => {
        // Remove the folder and files that holds report info
        await fs.emptyDirSync(path.join(__dirname, '../logs'));
        await fs.emptyDirSync(path.join(__dirname, '../reports'));
    },
    before: async () =>  {
        await browser.overwriteCommand('click', async function overwrite(origClickFunction, { highlightElement = true } = {}) {
            if (highlightElement) {
                const element = await $(this.selector);
                await browser.execute('arguments[0].style.border = "2px solid red"; arguments[0].style["border-style"] = "dashed";', element);
                await browser.pause(300);
                await browser.execute('arguments[0].style.border = "";', element);
            }
            return origClickFunction();
        }, true);
        await browser.setWindowSize(1920, 1080);
        await browser.maximizeWindow();
    },
    onComplete: async (exitCode, config) => {
        const options = {
            jsonDir: 'reports/cjson',
            reportPath: 'reports/html',
            pageTitle: 'Web Test Automation Results',
            reportName: 'Web Test Automation Results',
            pageFooter: '<div align="center"><p>Southern Code Challenge</p></div>',
            customData: {
                title: 'Test Context',
                data: [
                    { label: 'Environment', value: config.baseUrl },
                    { label: 'Execution Start time', value: testStart },
                    { label: 'Execution End time', value: new Date().toISOString() },
                    { label: 'Total Execution time', value: (new Date() - new Date(testStart)) / 1000 + 's' },
                ],
            },
        };

        reporter.generate(options);
    },

    async afterStep(uri, feature, { passed }) {
        if (!passed) {
            try {
                cucumberJson.attach(await browser.takeScreenshot(), 'image/png');
            } catch (error) {
                logger.error(`[ERROR] Error at taking screenshot:\n${error}`);
            }
        }
    },
};
