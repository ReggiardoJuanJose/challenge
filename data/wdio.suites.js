const path = require('path');

module.exports.suites = {
    allTests: [
        path.join(__dirname, '../tests/web/features/**/*.feature')
    ]
};
