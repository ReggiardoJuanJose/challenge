module.exports.locales = {
    English: 'en_US',
    Español: 'es_AR',
    Esperanto: 'eo_XX'
};

module.exports.labels = {
    English: 'Language',
    Español: 'Idioma',
    Esperanto: 'Lingvo'
};
