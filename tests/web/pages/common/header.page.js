
class HeaderPage {
    get searchBar() { return $('[id^=search_form_input]'); }
    get searchButton() { return $('[id^=search_button]'); }
    get privacyMenuButton() { return $('#wedonttrack'); }
    get socialMenuButton() { return $('span[data-type="social"]'); }
    get settingsMenuButton() { return $('.js-side-menu-open'); }
    get closeSettingsButton() { return $('.js-side-menu-close'); }

    async searchTerm(term) {
        await this.searchBar.setValue(term);
        await this.searchButton.click();
    }
    async openMenu(menu) {
        const menuOption = {
            privacy: await this.privacyMenuButton,
            social: await this.socialMenuButto,
            settings: await this.settingsMenuButton
        };
        await menuOption[menu].click();
    }
    async selectSettingsOption(option) {
        const settingsOptions = {
            Themes: '/settings#theme',
            'All Settings': '/settings',
        };
        await $(`a[href='${settingsOptions[option]}']`).click();
    }
}

module.exports = HeaderPage;
