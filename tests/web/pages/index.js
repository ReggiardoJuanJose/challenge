exports.HeaderPage = require('./common/header.page');
exports.HomePage = require('./home.page');
exports.ResultsPage = require('./results.page');
exports.SettingsPage = require('./settings.page');
