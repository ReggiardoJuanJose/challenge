const HeaderPage = require('./common/header.page');

class ResultsPage extends HeaderPage {
    get searchTypeBar() { return $('#duckbar'); }
    get searchFilters() { return $('.search-filters-wrap'); }
    // Main results
    get results() { return $$('#links article'); }
    get sidebarResult() { return $('.js-about-item'); }
    // More/Expand results
    get showMoreResultsButton() { return $('.result--more__btn'); }
    get showMoreResultsloader() { return $('.loader'); }

    async waitForPageToLoad() {
        await this.searchTypeBar.waitForDisplayed();
        await this.searchFilters.waitForDisplayed();
        await browser.waitUntil(
            async () => await this.results.length > 0,
            {
                timeout: 5000,
                timeoutMsg: 'Results were not displayed'
            }
        );
    }
    /**
    * Find a result in search results list with optional load more results when not found
    * @param {String} expectedResult
    * @param {String} options.loadMore
    * @param {Number} options.times
    * @returns {Promise<WebdriverIO.Element>}
    */
    async findExpectedResult(expectedResult, options = {}) {
        const find = () => this.results.find(async (result) => (await result.getText()).includes(expectedResult));

        let result = await find();
        if (options.loadMore) {
            let currentLoad = 0;
            while (currentLoad < options.times && !result) {
                await this.loadMoreResults();
                result = await find();
                currentLoad++;
            }
        }
        return result;
    }
    async loadMoreResults() {
        await this.showMoreResultsButton.scrollIntoView();
        await this.showMoreResultsButton.click();
        await this.showMoreResultsloader.waitForDisplayed({ reverse: true });
    }
}

module.exports = new ResultsPage();