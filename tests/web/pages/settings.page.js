const { locales } = require('../../../data/constants');
const HeaderPage = require('./common/header.page');

const THEMES_ID = { Terminal: 't'};

class SettingsPage extends HeaderPage {
    get settingsHeader() { return $('.set-head'); }
    get generalTab() { return $('[href="#general"]'); }
    get themeTab() { return $('[href="#theme"]'); }
    get appearanceTab() { return $('[href="#appearance"]'); }
    get privacyTab() { return $('[href="#privacy"]'); }
    get settingsDetails() { return $('.set-detail'); }
    // General Settings locators
    get languageLabel() { return $('//div[*[@id="setting_kad"]]/following-sibling::p'); }
    get languagesDropdown() { return $('#setting_kad'); }
    async getlanguageOption(language) { return $(`option[value=${language}]`); }
    // Theme locators
    async getTheme(themeName) { return $(`[data-theme-id=${ THEMES_ID[themeName] }]`); }
    get saveThemeButton() { return $('.js-set-exit'); }

    async waitForPageToLoad() {
        await this.settingsHeader.waitForDisplayed({
            timeout: 10000,
            timeoutMsg: 'Settings page was not displayed in expected time.'
        });
        return this.settingsDetails.waitForDisplayed();
    }
    async setLanguage(language) {
        await this.languagesDropdown.click();
        await (await this.getlanguageOption( locales[language] )).click();
    }
    async setTheme(theme) {
        await (await this.getTheme(theme)).click();
        await this.saveThemeButton.click();
    }
}

module.exports = new SettingsPage();