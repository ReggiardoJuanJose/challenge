const HeaderPage = require('./common/header.page');

class HomePage extends HeaderPage {
    get homeLogo() { return $('#logo_homepage_link'); }

    async waitForPageToLoad() {
        await this.homeLogo.waitForDisplayed({
            timeout: 15000,
            timeoutMsg: 'Home page logo was not displayed in expected time.'
        });
        return this.searchBar.waitForDisplayed({
            timeoutMsg: 'Searchbar was not displayed in expected time.'
        });
    }
}

module.exports = new HomePage();
