const { Given, When } = require('@cucumber/cucumber');
const { HomePage, SettingsPage } = require('../../pages');

const landingPages = {
    home: { pageObject: HomePage, path: '/'},
    settings: { pageObject: SettingsPage, path: '/settings'},
};

Given(/^DuckDuckGo (.*) page is displayed/, async (page) => {
    const Page = landingPages[page].pageObject;
    const path = landingPages[page].path;

    await browser.url(path);
    await Page.waitForPageToLoad();
});

When(/^user goes to (Themes|All Settings)$/, async (settingsOption) => {
    await HomePage.openMenu('settings');
    await HomePage.selectSettingsOption(settingsOption);
    await SettingsPage.waitForPageToLoad();
});
