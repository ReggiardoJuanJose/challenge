const { When, Then } = require('@cucumber/cucumber');
const { expect } = require('chai');
const { SettingsPage, HomePage } = require('../pages');
const { labels } = require('../../../data/constants');

let previousBackground;

When(/^user selects (Terminal|Esperanto) setting option$/, async (option) => {
    const settingOptions = {
        Terminal: async () => {
            previousBackground = await $('body').getCSSProperty('background');
            await SettingsPage.setTheme(option);
            await HomePage.waitForPageToLoad();
        },
        Esperanto: async () => await SettingsPage.setLanguage(option),
    };

    await settingOptions[option]();
});

Then(/^new (Theme|Language) is applied$/, async (setting) => {
    const settingsExpects = {
        Theme: async () => {
            const currentBackground = await $('body').getCSSProperty('background');
            expect(currentBackground.value).not.to.equal(previousBackground.value);
        },
        Language: async () => {
            expect(await SettingsPage.languageLabel.getText()).to.equal( labels['Esperanto'] );
        }
    };

    await settingsExpects[setting]();
});