const { When, Then } = require('@cucumber/cucumber');
const { expect } = require('chai');
const { HomePage, ResultsPage } = require('../pages');

When(/^user searches for (.*)$/, async (searchTerm) => {
    await HomePage.searchBar.setValue(searchTerm);
    await HomePage.searchButton.click();
    await ResultsPage.waitForPageToLoad();
});

// eslint-disable-next-line no-unused-vars
Then(/^a (.*) picture is displayed$/, async (expectedPicture) => {
    const image = await (await ResultsPage.sidebarResult).$('.module__image');
    expect(await image.isDisplayed()).to.be.true;
});

Then(/^a (.*) result is listed$/, async (expectedResult) => {
    expect(
        await ResultsPage.findExpectedResult(expectedResult, { loadMore: true, times: 2 })
    ).to.exist;
});