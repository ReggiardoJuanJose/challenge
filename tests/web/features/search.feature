Feature: Search in DuckDuckGo page tests

    Scenario: (TC-SE01) User can search for Michael Jordan
        Given DuckDuckGo home page is displayed
        When user searches for Michael Jordan
        Then a Michael Jordan picture is displayed
        And a wikipedia result is listed
        And a nba.com result is listed