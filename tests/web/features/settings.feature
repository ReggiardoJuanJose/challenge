Feature: Update DuckDuckGo page settings tests

    Background:
        Given DuckDuckGo home page is displayed

    Scenario Outline: <Id> User can update DuckDuckGo page <Setting>
        When user goes to <Tab>
        And user selects <Option> setting option
        Then new <Setting> is applied

    Examples:
        | Id        | Tab          | Setting  | Option    |
        | (TC-ST01) | Themes       | Theme    | Terminal  |
        | (TC-ST02) | All Settings | Language | Esperanto |
