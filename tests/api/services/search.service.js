require('dotenv').config();
const axios = require('axios');
const { assert } = require('chai');
const { validateResponseSchema } = require('../../utils/validator');
const { SearchResultsSchema } = require('../models/search.model');

const URL = process.env.API_URL;

class SearchService {
    async search(searchTerm) {
        const params = {
            q:searchTerm,
            format: 'json',
            pretty: 1,
        };
        try {
            const { status, data } = await axios.get(URL, { params });

            assert.equal(status, 200);
            await validateResponseSchema(data, SearchResultsSchema);
            return data;
        } catch (error) {
            assert.fail(`Error at searching for ${searchTerm}:\n${error}`);
        }
    }
}

module.exports = new SearchService();