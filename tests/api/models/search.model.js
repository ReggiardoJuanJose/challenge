module.exports.SearchResultsSchema = {
    'type': 'object',
    'properties': {
        'Abstract': {'type': 'string'},
        'AbstractSource': {'type': 'string'},
        'AbstractText': {'type': 'string'},
        'AbstractURL': {'type': 'string'},
        'Answer': {'type': 'string'},
        'AnswerType': {'type': 'string'},
        'Definition': {'type': 'string'},
        'DefinitionSource': {'type': 'string'},
        'DefinitionURL': {'type': 'string'},
        'Entity': {'type': 'string'},
        'Heading': {'type': 'string'},
        'Image': {'type': 'string'},
        'ImageHeight': {'type': 'number'},
        'ImageIsLogo': {'type': 'number'},
        'ImageWidth': {'type': 'number'},
        'Infobox': {
            'oneOf': [
                {
                    'type': 'object',
                    'properties': {
                        'content': { 'type': 'array', 'items': {'$ref': '#/definitions/contentItem' }}
                    }
                },
                { 'type': 'string' }
            ]
        },
        'Redirect': {'type': 'string'},
        'RelatedTopics': {'type': 'array', 'items': {'$ref': '#/definitions/resultItem' }},
        'Results': {'type': 'array', 'items': {'$ref': '#/definitions/resultItem' }}
    },
    'required': ['Infobox', 'RelatedTopics', 'Results'],

    'definitions': {
        'contentItem': {
            'type': 'object',
            'properties': {
                'data_type' : {'type': 'string'},
                'label' : {'type': 'string'},
                'value' : {'type': 'any'},
                'wiki_order' : {'type': 'string'}
            },
        },
        'resultItem': {
            'type': 'object',
            'properties': {
                'FirstURL': {'type': 'string'},
                'Icon': {
                    'type': 'object',
                    'properties': {
                        'Height': {'type': 'any'},
                        'URL': {'type': 'string'},
                        'Width': {'type': 'any'}
                    }
                },
                'Result': {'type': 'string'},
                'Text': {'type': 'string'}
            }
        }
    }
};
