const fs = require('fs-extra');
const path = require('path');
const { assert } = require('chai');

before('Prepare run', async function () {
    try {
        await fs.emptyDirSync(path.join(__dirname, '../../logs'));
        await fs.emptyDirSync(path.join(__dirname, '../../reports'));
    } catch(error) {
        assert.fail('Error on before hook:\n' + error);
    }
});
