require('../rootHooks');

const { describe, it, afterEach } = require('mocha');
const { flatten } = require('flat');
const addContext = require('mochawesome/addContext');
const logger = require('../../utils/logger');
const SearchService = require('../services/search.service');

describe('Search with DuckDuckGo Instant Answer API', async () => {
    let filteredResults;
    const searches = [
        { searchTerm: 'dogs', filterExpression: new RegExp('.*.(png|jpg|bmp|ico)$') },
        { searchTerm: 'dogecoin', filterExpression: '^http.*' }
    ];

    afterEach('Add context to mochawesome',async function() {
        await addContext(this, {
            title: 'Request filtered results',
            value: JSON.stringify(filteredResults, null, 4)
        });
    });

    searches.forEach((search) => {
        it(`Search for '${search.searchTerm}' and print results`, async function() {
            const results = await SearchService.search(search.searchTerm);

            filteredResults = await Object.values( flatten(results) )
                .filter(result => String(result).match(search.filterExpression));
            logger.info(JSON.stringify(filteredResults, null, 4));
        });
    });
});