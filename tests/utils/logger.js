const { createLogger, format, transports } = require('winston');
const { combine, printf } = format;

const myFormat = printf(({ level, message, timestamp }) => `${timestamp} [${level.toUpperCase()}]: ${message}`);

const logger = createLogger({
    format: combine(
        format.timestamp(),
        myFormat
    ),
    transports: [
        new transports.Console(),
    ],
});

module.exports = logger;
