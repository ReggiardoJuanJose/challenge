# Challenge Automation Framework

## Table of Contents
1. [Tools](#tools)
2. [First Steps](#first-steps)
3. [Folders](#folders)
4. [Web Framework](#web-framework)
    * [Gherkin Feature Definitions](#gherkin-feature-definitions)
    * [Step Definitions](#step-definitions)
    * [Page Objects](#page-objects)
5. [API framework](#api-framework)
    * [Models](#models)
    * [Services](#services)
    * [Specs](#specs)
6. [How to run tests](#how-to-run-tests)
    * [Web](#web)
    * [API](#api)
7. [Environment Variables](#environment-variables)
8. [Test Results Report](#test-results-report)

<a name="tools"></a>

## Tools

* WebDriverIO
* CucumberJS
* MochaJS
* Axios
* jsonschema

<a name="first-steps"></a>

## First Steps

1. Clarifications
    * This project consists of 2 frameworks since WebdriverIO doesn't support standalone api testing without launching a browser.
    * For web tests a chromium binary is downloaded and used in order to match version with chromedriver so as to avoid running issues if updating packages or new installations. Set chromium version in `.npmrc` file matching `./package.json` chromedriver version.

2. Prerequisites:
    * This suite should run using at least Node version 14.
    * You need to have firefox installed in order to run tests in that browser.

3. Install and configuration:
    * Run `npm install` in your local environment root folder.
    * Update _MAX_INSTANCES_ in `.env` file to change workers number if needed.

<a name="folders"></a>

## Folders

* `config`: The framework configuration files.
    * `/wdio.conf.js`: Default configuration from wdio init used as reference only.
    * `/cucumber.conf.js`: Common configuration for running web tests.
    * `/{browser}.conf.js`: Per browser capabilities, they extend `cucumber.conf.js` so call this for running tests in selected browser.
    * `/mocha.conf.js`: Mocha configuration for running API tests.
* `data`: Constants and parameters definitions.
* `tests`: Common folder for API/Web tests and needed tools.

<a name="web-framework"></a>

## Web Framework

This frameworks is based on [WebdriverIO](https://webdriver.io/)

It implements:
* [Cucumber](https://www.npmjs.com/package/@wdio/cucumber-framework) as the testing framework.
* The [Page Object](https://martinfowler.com/bliki/PageObject.html) design pattern.
* [ChaiJS](https://www.chaijs.com/) as the assertion library.

<a name="gherkin-feature-definitions"></a>

### Gherkin Feature Definitions

[Gherkin](https://cucumber.io/docs/gherkin/reference/) files are found in the [`./tests/web/features`](./tests/web/features) folder.

Those are the features-under-test definition in a human readable language.

<a name="step-definitions"></a>

### Step Definitions

The [step definitions](https://cucumber.io/docs/cucumber/step-definitions/) files are found in the [`./tests/web/steps`](./tests/web/steps) folder.

This steps are the scripts that _translate_ Gherkin steps into executable code.

<a name="services"></a>

### Services

The services files are shared with api ones if needed and are found in the [`./tests/api/services`](./tests/api/services) folder.

The services are used to set test preconditions, for example Authenticate a user through Auth services, instead of _manual login_.

<a name="page-objects"></a>

### Page Objects

The [Page Objects](https://martinfowler.com/bliki/PageObject.html) are found in the [`./tests/web/pages`](./tests/web/pages) folder.

Martin Fowler's definition
> When you write tests against a web page, you need to refer to elements within that web page in order to click links and determine what's displayed. However, if you write tests that manipulate the HTML elements directly your tests will be brittle to changes in the UI. A page object wraps an HTML page, or fragment, with an application-specific API, allowing you to manipulate page elements without digging around in the HTML.

<a name="apiframework"></a>

## API framework

This frameworks is based on [MochaJS](https://mochajs.org/)

It implements:
* [Axios](https://www.npmjs.com/package/axios) for making requests.
* [jsonschema](https://www.npmjs.com/package/jsonschema) for responses validation.
* [ChaiJS](https://www.chaijs.com/) as the assertion library.

<a name="models"></a>

### Models

Schemas for validating responses against.

<a name="services"></a>

### Services

The services files are shared with web ones if needed and are found in the [`./tests/api/services`](./tests/api/services) folder.

The services are used to make requests or set test preconditions and make automation easier.

<a name="specs"></a>

### Specs

Spec files are found in the [`./tests/api/specs`](./tests/api/specs) folder.

Those are test cases defined, grouped by app or test scope.

<a name="how-to-run-tests"></a>

## How to run tests

<a name="web"></a>

### Web

1. To run a particular Feature file run `testChrome|testFirefox` script with `--spec` argument followed by path to feature file

        npm run testChrome -- --spec YOUR_FEATURE_FILE
        npm run testChrome -- --spec ./tests/web/features/search.feature

2. To run a test suite run script with `--suite` argument followed by a suite listed in [`./data/wdio.suites.js`](./data/wdio.suites.js)

        npm run testChrome -- --suite YOUR_TEST_SUITE
        npm run testChrome -- --suite allTests

<a name="api"></a>

### API

1. To run a particular Spec file run `mocha` script followed by path to spec file

        npm run mocha YOUR_SPEC_FILE
        npm run mocha ./tests/api/specs/search.spec.js

2. To run a test suite run `mocha` script followed by  path of a file in [`./data/mochaSuites`](./data/mochaSuites)

        `npm run mocha YOUR_SUITE_FILE`
        `npm run mocha ./data/mochaSuites/allTests.js`

<a name="envvars"></a>

## Environment Variables

The following environment variables are **mandatory**:
* `BASE_URL`: the URL of the web environment under test. It should end WITHOUT the backslash.
* `API_URL`: the host URL for api requests. It should end WITHOUT the backslash

The following environment variables are optional:
* `MAX_INSTANCES`: Number of workers running web tests

<a name="test-results-report"></a>

## Test Results Report

Once tests are completed open:
* Web: `./reports/html/index.html` to get the results report
* API: `./reports/mochawesome/mochawesome.html` to get the results report
